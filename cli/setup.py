import setuptools

setuptools.setup(
    name="bookstore",
    version="0.0.1",
    author="Ryan Sheppard",
    author_email="ryan.sheppard@nielsen.com",
    description="the premier method of buying books",
    long_description="no",
    long_description_content_type="text/markdown",
    packages=["bookstore"],
    classifiers=["Programming Language :: Python :: 3"],
    entry_points={"console_scripts": ["bookstore=bookstore.bookstore:main"]},
)
