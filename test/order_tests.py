import falcon.testing as testing
import api
import falcon
import json


client = testing.TestClient(api.create())


# get orders
def test_get_orders():
    response = client.simulate_request(path='/orders', method='GET')
    values = set()
    for i in response.json:
        values.add(i['orderid'])
    assert len(values) == 30
    assert response.status == falcon.HTTP_200

    id = values.pop()
    response = client.simulate_request(path=f'/orders/{id}', method='GET')
    assert response.status == falcon.HTTP_200


def test_invalid_orders():
    response = client.simulate_request(path='/orders/1', method='GET')
    assert not response.json


def test_put_invalid_orders():
    response = client.simulate_request(path='/orders', method='POST')
    assert response.status == falcon.HTTP_500


test_get_orders()
test_invalid_orders()
test_put_invalid_orders()
