FROM python:3.7.4-alpine

RUN apk add --no-cache uwsgi-python3
RUN mkdir /app/
COPY *.py /app/
COPY requirements.txt /app
WORKDIR /app/
RUN pip install -r requirements.txt

EXPOSE 8000
ENTRYPOINT ["python", "api.py"]
