import falcon.testing as testing
import falcon
import json
import api

client = testing.TestClient(api.create())


def test_get_all_users():
    response = client.simulate_request(path='/users', method='GET')
    assert response.status == falcon.HTTP_200
    assert len(response.json) == 20


def test_get_user_invalid():
    response = client.simulate_request(path='/users/1', method='GET')
    assert response.status == falcon.HTTP_200
    assert len(response.json) == 0


def test_post_user():
    body = {
        "username": "Travis DeMint"
    }
    response = client.simulate_request(path='/users', headers={"Content-Type": "application/json"}, body=json.dumps(body), method='POST')
    assert response.status == falcon.HTTP_200
    assert len(response.json) == 1
    assert response.json[0]["username"] == "Travis DeMint"

    uuid = response.json[0]["id"]

    response = client.simulate_request(path=f'/users/{uuid}', method='GET')
    assert response.status == falcon.HTTP_200
    assert len(response.json) == 1

    client.simulate_request(path=f'/users/{uuid}', method='DELETE')


def run_user_tests():
    test_get_all_users()
    test_get_user_invalid()
    test_post_user()
