from dataclasses import dataclass, asdict
from neo4j import GraphDatabase
import falcon
import json
import uuid


@dataclass
class User:
    username: str
    id: str

    @staticmethod
    def _format_results(results):
        users = []
        for result in results:
            username = result["u"]["username"]
            userid = result["u"]["userID"]
            user = User(username=username, id=userid)
            users.append(asdict(user))

        return json.dumps(users)


class UserResource:
    def __init__(self, uri, user, password):
        self._driver = GraphDatabase.driver(uri, auth=(user, password))

    def on_get(self, req, resp, user_id=None):
        try:
            if user_id:
                with self._driver.session() as session:
                    user = session.write_transaction(self._get_user_by_id, user_id)
                    results = User._format_results(user)
                    resp.status = falcon.HTTP_200
                    resp.body = str(results)
            else:
                with self._driver.session() as session:
                    user_list = session.write_transaction(self._get_user_list)
                    results = User._format_results(user_list)
                    resp.status = falcon.HTTP_200
                    resp.body = str(results)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_post(self, req, resp):
        try:
            with self._driver.session() as session:
                user = json.loads(req.bounded_stream.read())

                user_resp = session.write_transaction(self._post_user, user["username"])
                results = User._format_results(user_resp)
                resp.status = falcon.HTTP_200
                resp.body = str(results)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_delete(self, req, resp, user_id=None):
        try:
            if user_id:
                with self._driver.session() as session:
                    user_resp = session.write_transaction(self._delete_user, user_id)
                    #results = User._format_results(user_resp)
                    resp.status = falcon.HTTP_200
                    resp.body = str(user_resp)
            else:
                resp.status = falcon.HTTP_400
                resp.body = "Please provide user_id"
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    @staticmethod
    def _get_user_list(tx):
        result = tx.run("MATCH (u:User) return u")
        return result

    @staticmethod
    def _get_user_by_id(tx, user_id):
        result = tx.run(f"MATCH (u:User {{userID:'{user_id}'}}) RETURN u")
        return result

    @staticmethod
    def _post_user(tx, name):
        user_id = uuid.uuid1()
        result = tx.run(f"CREATE (u:User {{userID:'{user_id}', username:'{name}'}}) RETURN u")
        return result

    @staticmethod
    def _delete_user(tx, user_id):
        tx.run(f"MATCH (u:User {{userID:'{user_id}'}}) DETACH DELETE (u)")
        return "User successfully deleted!"
