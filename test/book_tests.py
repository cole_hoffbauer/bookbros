import falcon.testing as testing
import falcon
import api
import json

client = testing.TestClient(api.create())


# get all books
def test_get_all_books():
    response = client.simulate_request(path='/books', method='GET')
    assert response.status == falcon.HTTP_200
    assert len(response.json) == 10


# valid isbn
def test_get_isbn_book():
    response = client.simulate_request(path='/books/1', method='GET')
    assert response.status == falcon.HTTP_200
    assert response.json[0] == {"title": "ut", "isbn": 1, "author": "Kevon Olson", "publisher": "saepe"}


# invalid isbn
def test_get_invalid_isbn_book():
    response = client.simulate_request(path='/books/2', method='GET')
    assert not response.json
    assert response.status == falcon.HTTP_200


# get book by author
def test_get_author_book():
    response = client.simulate_request(path='/books', params={"author": "Eleonore Macejkovic"}, method='GET')
    assert response.status == falcon.HTTP_200
    assert response.json[0] == {"title": "dolore", "isbn": 85937, "author": "Eleonore Macejkovic", "publisher": "maiores"}


# get book by author and publisher
def test_get_author_publisher_book():
    response = client.simulate_request(path='/books', params={"author": "Eleonore Macejkovic", "publisher": "maiores"}, method='GET')
    assert response.status == falcon.HTTP_200
    assert response.json[0] == {'title': 'dolore', 'isbn': 85937, 'author': 'Eleonore Macejkovic', 'publisher': 'maiores'}


# get book by title
def test_get_title_book():
    response = client.simulate_request(path='/books', params={"title": "et"}, method='GET')
    assert response.status == falcon.HTTP_200
    assert response.json[0] == {"title": "et", "isbn": 140031, "author": "Kevon Olson", "publisher": "saepe"}
    assert response.json[1] == {"title": "et", "isbn": 94685044, "author": "Erna Gorczany", "publisher": "maiores"}


# get book by invalid title
def test_get_invalid_title_book():
    response = client.simulate_request(path='/books', params={"title": "test"}, method='GET')
    assert response.status == falcon.HTTP_200
    assert not response.json


# post a book
def test_post_book():
    book = {
        "title": "Book",
        "isbn": "123456",
        "author": "Willy",
        "publisher": "Pubster"
    }
    response = client.simulate_request(path='/books', headers={"Content-Type": "application/json"}, body=json.dumps(book), method='POST')
    assert response.json[0] == {"title": "Book", "isbn": 123456, "author": "Willy", "publisher": "Pubster"}
    assert response.status == falcon.HTTP_200


# delete created book
def test_delete_book():
    response = client.simulate_request(path='/books/123456', method='DELETE')
    assert response.text == "Book successfully deleted!"
    assert response.status == falcon.HTTP_200


# invalid book add
def test_invalid_post_book():
    body = {
        "isbn": "123",
        "publisher": "Pubster"
    }
    # invalid book add
    response = client.simulate_request(path='/books', headers={"Content-Type": "application/json"}, body=json.dumps(body), method='POST')
    assert response.status == falcon.HTTP_500


# delete invalid book
def test_delete_invalid_book():
    response = client.simulate_request(path='/books', method='DELETE')
    assert response.status == falcon.HTTP_400


# run all tests
def run_book_tests():
    test_get_all_books()
    test_get_isbn_book()
    test_get_invalid_isbn_book()
    test_get_author_book()
    test_get_author_publisher_book()
    test_get_title_book()
    test_get_invalid_title_book()
    test_post_book()
    test_delete_book()
    test_invalid_post_book()
    test_delete_invalid_book()
