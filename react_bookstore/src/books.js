import React from 'react'

const Books = ({ books }) => {
  return (
    <div >
      <style>{'body { background-color: lightblue; }'}</style>
      <center><h1>Books</h1></center>
      {books.map((book) => (
            <h3>Book title:{book.title} Author:{book.author} ISBN:{book.isbn}</h3>
      ))}
    </div>
  )
};

export default Books