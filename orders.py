from neo4j import GraphDatabase
import falcon
import json
import uuid
from dataclasses import dataclass, asdict, field
from books import Book
from typing import List


@dataclass
class Order:
    orderid: str
    username: str
    status: str = 'PENDING'
    books: List[Book] = field(default_factory=list)

    @staticmethod
    def _format_results(results):
        orders_list = []
        books = {}
        results_copy = results.records()

        for result in results_copy:
            orderid = result["o"]["orderID"]
            orderstatus = result["o"]["status"]
            try:
                book = result["b"]
            except Exception:
                book = None

            if orderid not in books.keys():
                books[orderid] = []
            if book:
                books[orderid].append(Book(title=book.get('title'), isbn=book.get('isbn')))

            user = result["u"]["username"]
            order = Order(orderid=orderid, username=user, status=orderstatus)
            orders_list.append(order)

        orders = []
        for order in orders_list:
            order.books = books[order.orderid]
            orders.append(asdict(order))
        return json.dumps(orders)


class OrderResource:
    def __init__(self, uri, user, password):
        self._driver = GraphDatabase.driver(uri, auth=(user, password))

    def on_get(self, req, resp, order_id=None):
        try:
            if order_id:
                with self._driver.session() as session:
                    order = session.write_transaction(self._get_order_by_id, order_id)
                    results = Order._format_results(order)
                    resp.status = falcon.HTTP_200
                    resp.body = str(results)
            else:
                with self._driver.session() as session:
                    user_id = req.get_param("userID", required=False)
                    order_list = session.write_transaction(self._get_order_list, user_id)
                    results = Order._format_results(order_list)
                    resp.status = falcon.HTTP_200
                    resp.body = str(results)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_post(self, req, resp):
        try:
            with self._driver.session() as session:
                order = json.loads(req.bounded_stream.read())

                order_resp = session.write_transaction(self._post_order, order["userID"])
                results = Order._format_results(order_resp)
                resp.status = falcon.HTTP_200
                resp.body = str(results)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_put(self, req, resp):
        try:
            with self._driver.session() as session:
                order = json.loads(req.bounded_stream.read())

                if order["status"] == "COMPLETED" or order["status"] == "PENDING":
                    order_resp = session.write_transaction(self._update_order, order["orderID"], order["status"])
                else:
                    resp.status = falcon.HTTP_400
                    resp.body = "Please enter a valid status"

                resp.status = falcon.HTTP_200
                resp.body = str(order_resp)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_delete(self, req, resp, order_id=None):
        try:
            if order_id:
                with self._driver.session() as session:
                    order_resp = session.write_transaction(self._delete_order, order_id)
                    #results = Order._format_results(order_resp)
                    resp.status = falcon.HTTP_200
                    resp.body = str(order_resp)
            else:
                resp.status = falcon.HTTP_400
                resp.body = "Please provide order_id"
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_patch(self, req, resp, order_id=None, book_id=None):
        try:
            if order_id and book_id:
                with self._driver.session() as session:
                    path = req.uri

                    if "add" in path:
                        add_resp = session.write_transaction(self._add_book, order_id, book_id)

                        resp.status = falcon.HTTP_200
                        resp.body = str(add_resp)
                    elif "remove" in path:
                        remove_resp = session.write_transaction(self._remove_book, order_id, book_id)

                        resp.status = falcon.HTTP_200
                        resp.body = str(remove_resp)
                    else:
                        resp.status = falcon.HTTP_400
                        resp.body = "Please send a valid path"
            else:
                resp.status = falcon.HTTP_400
                resp.body = "Please provide order_id and book_id"
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    @staticmethod
    def _get_order_list(tx, user_id):
        if user_id:
            result = tx.run(
                f"MATCH (u:User {{userID:'{user_id}'}})-[:MAKES]->(o:Order)-[:CONTAINS]->(b:Book) RETURN u,o,b"
            )
        else:
            result = tx.run("MATCH (u:User)-[:MAKES]->(o:Order)-[:CONTAINS]->(b:Book) return o,u,b")

        return result

    @staticmethod
    def _get_order_by_id(tx, order_id):
        result = tx.run(
            f"MATCH (u:User)-[:MAKES]->(o:Order {{orderID:'{order_id}'}}) OPTIONAL MATCH (o)-[:CONTAINS]->(b:Book) return o,u,b"
        )
        return result

    @staticmethod
    def _post_order(tx, user_id):
        order_id = uuid.uuid1()
        results = tx.run(f"MATCH (u:User {{userID:'{user_id}'}}) CREATE (u)-[:MAKES]->(o:Order {{orderID:'{order_id}', status:'PENDING'}}) return o,u")
        return results

    @staticmethod
    def _update_order(tx, order_id, status):
        tx.run(f"MATCH (o:Order {{orderID:'{order_id}'}}) SET o.status='{status}' RETURN o")
        return "Order updated successfully"

    @staticmethod
    def _delete_order(tx, order_id):
        tx.run(f"MATCH (o:Order {{orderID:'{order_id}'}}) DETACH DELETE (o)")
        return "Order successfully deleted!"

    @staticmethod
    def _add_book(tx, order_id, book_id):
        tx.run(f"MATCH (o:Order {{orderID:'{order_id}'}}) MATCH (b:Book {{isbn:{book_id}}}) CREATE (o)-[:CONTAINS]->(b)")
        return "Book successfully added to order"

    @staticmethod
    def _remove_book(tx, order_id, book_id):
        tx.run(f"MATCH (o:Order {{orderID:'{order_id}'}})-[r:CONTAINS]->(b:Book {{isbn:{book_id}}}) DELETE r")
        return "Book successfully removed from order"
