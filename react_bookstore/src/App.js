import React, { Component } from 'react';
import Books from './books';

class App extends Component {
  
  state = {
    books: []
  }

  componentDidMount() {
    fetch('http://localhost:8000/books')
    .then(res => res.json())
    .then((data) => {
      this.setState({ books: data })
    })
    .catch(console.log)
  }
  
  render() {
    return (
    <Books books={this.state.books}></Books>
    );
  }
}

export default App;




