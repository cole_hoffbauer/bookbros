import os
from wsgiref.simple_server import make_server
import falcon

from books import BookResource
from orders import OrderResource
from users import UserResource

NEO4J_DATABASE_URI = os.environ.get("NEO4J_DATABASE_URI", "bolt://localhost:7687")


def create():
    app = falcon.API()

    # Resources are represented by long-lived class instances
    books = BookResource(NEO4J_DATABASE_URI, "neo4j", "yes")
    orders = OrderResource(NEO4J_DATABASE_URI, "neo4j", "yes")
    users = UserResource(NEO4J_DATABASE_URI, "neo4j", "yes")

    app.add_route("/books", books)
    app.add_route("/books/{isbn}", books)
    app.add_route("/orders", orders)
    app.add_route("/orders/{order_id}", orders)
    app.add_route("/orders/{order_id}/add/{book_id}", orders)
    app.add_route("/orders/{order_id}/remove/{book_id}", orders)
    app.add_route("/users", users)
    app.add_route("/users/{user_id}", users)

    return app


def main():
    with make_server("", 8000, create()) as httpd:
        print("Serving on port 8000...")

        # Serve until process is killed
        httpd.serve_forever()


if __name__ == "__main__":
    main()
