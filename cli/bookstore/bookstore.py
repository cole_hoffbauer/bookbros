import os
from pathlib import Path
import sys
import argparse
import requests


def login_user(user_id):
    home = str(Path.home())
    with open(os.path.join(home, ".bookstore"), "w") as f:
        f.write(str(user_id))

    print(f"Logged in as {user_id}")


def get_user_id():
    home = str(Path.home())
    with open(os.path.join(home, ".bookstore"), "r") as f:
        return f.read().strip()


def create_book(isbn, title, author, publisher):
    json_blob = {"isbn": isbn, "title": title, "author": author, "publisher": publisher}
    r = requests.post("http://localhost:8000/books", json=json_blob)
    if r.status_code == requests.codes.ok:
        print("Book created!")
    else:
        print("failed to create book")


def delete_book(isbn):
    r = requests.delete(f"http://localhost:8000/books/{isbn}")
    if r.status_code == requests.codes.ok:
        print("Book deleted!")
    else:
        print("failed to delete book")


def list_books():
    r = requests.get("http://localhost:8000/books")
    books = r.json()
    print(f"{'ISBN':<20}{'Title':<20}{'Author':<20}Publisher")
    for book in books:
        print(
            f"{book['isbn']:<20}{book['title']:<20}{book['author']:<20}{book['publisher']}"
        )


def get_book(resource_id):
    if resource_id.isdigit():
        r = requests.get(f"http://localhost:8000/books/{resource_id}")
    else:
        r = requests.get(f"http://localhost:8000/books?title={resource_id}")
    try:
        book = r.json()[0]
    except:
        print("Book not found")
        sys.exit(1)

    print(
        f"ISBN: {book['isbn']}\nTitle: {book['title']}\nAuthor: {book['author']}\nPublisher: {book['publisher']}"
    )


def list_orders():
    r = requests.get("http://localhost:8000/orders")
    orders = r.json()
    print(f"{'Order ID':<50}{'Username':<20}{'Status':<20}{'Books':<20}")
    for order in orders:
        books = [book["title"] for book in order["books"]]
        book_list = ", ".join(books)
        status = order["status"] if order["status"] else "PENDING"
        print(f"{order['orderid']:<50}{order['username']:<20}{status:<20}{book_list}")


def get_order(order_id):
    r = requests.get(f"http://localhost:8000/orders/{order_id}")
    try:
        order = r.json()[0]
    except:
        print("Order not found")
        sys.exit(1)
    books = [book["title"] for book in order["books"]]
    book_list = ", ".join(books)
    print(
        f"Order ID: {order['orderid']}\nUsername: {order['username']}\nBooks: {book_list}"
    )


def create_order():
    user_id = get_user_id()
    r = requests.post(f"http://localhost:8000/orders", json={"userID": user_id})
    order = r.json()[0]
    if r.status_code == requests.codes.ok:
        print(f"Order {order['orderid']} created")
    else:
        print("Failed to create")


def add_book(order_id, isbn):
    r = requests.patch(f"http://localhost:8000/orders/{order_id}/add/{isbn}")
    if r.status_code == requests.codes.ok:
        print(f"Added {isbn} to order {order_id}")
    else:
        print("Failed to add book")


def remove_book(order_id, isbn):
    r = requests.patch(f"http://localhost:8000/orders/{order_id}/remove/{isbn}")
    if r.status_code == requests.codes.ok:
        print(f"Removed {isbn} from order {order_id}")
    else:
        print("Failed to add book")


def list_users():
    r = requests.get("http://localhost:8000/users")
    users = r.json()
    print(f"{'Username':<20}")
    for user in users:
        print(f"{user['username']:<20}")


def get_user(user_id):
    r = requests.get(f"http://localhost:8000/users/{user_id}")
    try:
        user = r.json()[0]
    except:
        print("User not found")
        sys.exit(1)
    print(f"Username: {user['username']}")


def complete_order(order_id):
    r = requests.put(
        "http://localhost:8000/orders",
        json={"orderID": order_id, "status": "COMPLETED"},
    )
    if r.status_code == requests.codes.ok:
        print(
            "Ordered books. Thanks for using bookbros. Your order has been delivered."
        )
    else:
        print("Order failed, press up and repeat")


parser = argparse.ArgumentParser(description="bookstore")
subparsers = parser.add_subparsers(title="subcommands", dest="subcommand")
get_items = subparsers.add_parser("get")
get_items.add_argument("context", nargs="+")
create_items = subparsers.add_parser("create")
create_items.add_argument("context", nargs="+")
create_items.add_argument("--isbn")
create_items.add_argument("--title")
create_items.add_argument("--author")
create_items.add_argument("--publisher")
create_items.add_argument("--order-id")
delete_items = subparsers.add_parser("delete")
delete_items.add_argument("context", nargs="+")
delete_items.add_argument("--order-id")
delete_items.add_argument("--isbn")
login = subparsers.add_parser("login")
login.add_argument("--user-id")
add = subparsers.add_parser("add")
add.add_argument("context", nargs="+")
add.add_argument("--order-id")
add.add_argument("--isbn")
checkout = subparsers.add_parser("checkout")
checkout.add_argument("--order-id")

args = parser.parse_args()


def main():
    if args.subcommand == "get":
        resource = args.context[0]
        if resource in ("book", "books"):
            if len(args.context) == 1:
                list_books()
            else:
                resource_id = args.context[1]
                get_book(resource_id)
        elif resource in ("order", "orders"):
            if len(args.context) == 1:
                list_orders()
            else:
                resource_id = args.context[1]
                get_order(resource_id)
        elif resource in ("user", "users"):
            if len(args.context) == 1:
                list_users()
            else:
                resource_id = args.context[1]
                get_user(resource_id)
    elif args.subcommand == "create":
        resource = args.context[0]
        if resource in ("book", "books"):
            isbn = args.isbn
            title = args.title
            author = args.author
            publisher = args.publisher
            create_book(isbn, title, author, publisher)
        if resource in ("order", "orders"):
            create_order()
    elif args.subcommand == "add":
        resource = args.context[0]
        if resource in ("book", "books"):
            isbn = args.isbn
            order_id = args.order_id
            add_book(order_id, isbn)
    elif args.subcommand == "delete":
        resource = args.context[0]
        if resource in ("book", "books"):
            isbn = args.context[1]
            delete_book(isbn)
        elif resource in ("order", "orders"):
            isbn = args.isbn
            order_id = args.order_id
            remove_book(order_id, isbn)
    elif args.subcommand == "login":
        login_user(args.user_id)
    elif args.subcommand == "checkout":
        complete_order(args.order_id)


if __name__ == "__main__":
    main()
