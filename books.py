import json
from dataclasses import dataclass, asdict
from neo4j import GraphDatabase
import falcon
import json


@dataclass
class Book:
    title: str
    isbn: int
    author: str = ''
    publisher: str = ''

    @staticmethod
    def _format_results(results):
        books = []
        for result in results:
            author = result["a"]["name"]
            publisher = result["p"]["name"]
            title = result["b"]["title"]
            isbn = result["b"]["isbn"]
            book = Book(isbn=isbn, title=title, author=author, publisher=publisher)
            books.append(asdict(book))
        return json.dumps(books)


class BookResource:
    def __init__(self, uri, user, password):
        self._driver = GraphDatabase.driver(uri, auth=(user, password))

    def on_get(self, req, resp, isbn=None):
        try:
            if isbn:
                with self._driver.session() as session:
                    book = session.write_transaction(self._get_book_by_isbn, isbn)
                    results = Book._format_results(book)
                    resp.status = falcon.HTTP_200
                    resp.body = str(results)
            else:
                with self._driver.session() as session:
                    author = req.get_param("author", required=False)
                    publisher = req.get_param("publisher", required=False)
                    title = req.get_param("title", required=False)
                    book_list = session.write_transaction(
                        self._get_book_list, author, publisher, title
                    )
                    results = Book._format_results(book_list)
                    resp.status = falcon.HTTP_200
                    resp.body = str(results)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_post(self, req, resp):
        try:
            with self._driver.session() as session:
                book = json.loads(req.bounded_stream.read())

                book_resp = session.write_transaction(
                    self._post_book, book["isbn"], book["title"], book["author"], book["publisher"]
                )
                results = Book._format_results(book_resp)

                resp.status = falcon.HTTP_200
                resp.body = str(results)
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    def on_delete(self, req, resp, isbn=None):
        try:
            if isbn:
                with self._driver.session() as session:
                    order_resp = session.write_transaction(self._delete_book, isbn)
                    resp.status = falcon.HTTP_200
                    resp.body = str(order_resp)
            else:
                resp.status = falcon.HTTP_400
                resp.body = "Please provide isbn"
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = str(e)

    @staticmethod
    def _get_book_list(tx, author, publisher, title):
        if author:
            author = author.replace("%20", " ")
            author_string = f"{{name:'{author}'}}"
        else:
            author_string = ""

        if publisher:
            publisher = publisher.replace("%20", " ")
            publisher_string = f"{{name: '{publisher}'}}"
        else:
            publisher_string = ""

        if title:
            title = title.replace("%20", " ")
            title_string = f"{{title: '{title}'}}"
        else:
            title_string = ""

        result = tx.run(
            f"MATCH (a:Author{author_string})-[:WRITES]->(b:Book {title_string})<-[:PUBLISHES]-(p:Publisher{publisher_string}) return b,a,p"
        )
        return result

    @staticmethod
    def _get_book_by_isbn(tx, isbn):
        result = tx.run(
            "MATCH (a:Author)-[:WRITES]->(b:Book {isbn:$isbn})<-[:PUBLISHES]-(p:Publisher) return b,a,p",
            isbn=int(isbn),
        )
        return result

    @staticmethod
    def _post_book(tx, isbn, title, author, publisher):
        results = tx.run(
            f"CREATE (b:Book {{isbn:{isbn}, title:'{title}'}}) CREATE (a:Author {{name:'{author}'}}) CREATE (p:Publisher {{name:'{publisher}'}}) CREATE (a)-[:WRITES]->(b)<-[:PUBLISHES]-(p) return b,a,p"
        )
        return results

    @staticmethod
    def _delete_book(tx, isbn):
        tx.run(f"MATCH (b:Book {{isbn:{isbn}}}) DETACH DELETE (b)")
        return "Book successfully deleted!"
